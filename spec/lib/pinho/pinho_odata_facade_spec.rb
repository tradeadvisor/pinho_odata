require 'spec_helper'
require './lib/pinho/odata_facade'

module Pinho
  describe ODataFacade do

  	before :each do

 	 	stub_request(:get, "#{ENV['ODATA_API_URL']}/$metadata").
        	with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>"#{ENV['ODATA_API_HOST']}:80", 
        		'User-Agent'=>'Ruby'}).
         	to_return(:status => 200, :body => %Q{
         	<?xml version="1.0" encoding="utf-8"?>
				<edmx:Edmx Version="4.0" xmlns:edmx="http://docs.oasis-open.org/odata/ns/edmx">
				  <edmx:DataServices>
				    <Schema Namespace="ShipmentOData" xmlns="http://docs.oasis-open.org/odata/ns/edm">
				      <EntityType Name="Shipment">
				        <Key>
				          <PropertyRef Name="cd_lancamento" />
				          <PropertyRef Name="cd_teste" />
				        </Key>
				        <Property Name="cd_lancamento" Type="Edm.Int32" Nullable="false" />
				        <Property Name="cd_teste" Type="Edm.String" Nullable="false" />
				      </EntityType>
				    </Schema>
				    <Schema Namespace="Default" xmlns="http://docs.oasis-open.org/odata/ns/edm">
				      <EntityContainer Name="Container">
				        <EntitySet Name="Shipments" EntityType="ShipmentOData.Shipment" />
				      </EntityContainer>
				    </Schema>
				  </edmx:DataServices>
				</edmx:Edmx>
  		})

       stub_request(:get, "#{ENV['ODATA_API_URL']}/Shipments?$top=1").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>"#{ENV['ODATA_API_HOST']}:80", 
         	'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909
			    }
			  ]
			}
         })

       stub_request(:get, "#{ENV['ODATA_API_URL']}/Shipments").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>"#{ENV['ODATA_API_HOST']}:80", 
         	'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909
			    },
			    {
			      "cd_lancamento":21910
			    }
			  ]
			}
         })

        stub_request(:get, "http://api.odata.com/Shipments?$filter=cd_lancamento%20eq%2021909").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>'api.odata.com:80', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909
			    },
			    {
			      "cd_lancamento":21910
			    }
			  ]
			}
		})

        stub_request(:get, "http://api.odata.com/Shipments?$filter=cd_lancamento%20eq%2021909&$top=1").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>'api.odata.com:80', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909
			    },
			    {
			      "cd_lancamento":21910
			    }
			  ]
			}
		})

        stub_request(:get, "http://api.odata.com/Shipments?$filter=cd_teste%20eq%20'string_result'").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>'api.odata.com:80', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909,
			      "cd_teste": "string_result"
			    }
			  ]
			}
		})

      	stub_request(:get, "http://api.odata.com/Shipments?$orderby=cd_lancamento%20asc").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>'api.odata.com:80', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909
			    },
			    {
			      "cd_lancamento":21910
			    }
			  ]
			}
		})

        stub_request(:get, "http://api.odata.com/Shipments?$orderby=cd_lancamento%20desc").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>'api.odata.com:80', 'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909
			    },
			    {
			      "cd_lancamento":21910
			    }
			  ]
			}
		})

    end

     it "should return a valid connection" do
	 	conn = Pinho::ODataFacade.connection
	 end

	 it "should return a valid top search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade 
	 	end
	 	shipment = Shipment.top(1).first
	 	expect(shipment.attributes["cd_lancamento"]).to eq(21909)
	 end

	 it "should return a valid filter search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade 
	 	end
	 	shipment = Shipment.filter("cd_lancamento eq 21909").first
	 	expect(shipment.attributes["cd_lancamento"]).to eq(21909)
	 end

	 it "should return a valid where search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade 
	 	end
	 	shipment = Shipment.where({cd_teste: "string_result"}).first
	 	expect(shipment.attributes["cd_teste"]).to eq("string_result")
	 end


	 it "should return a valid chained search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade 
	 	end
	 	shipment = Shipment.filter("cd_lancamento eq 21909").top(1).first
	 	expect(shipment.attributes["cd_lancamento"]).to eq(21909)
	 end

	 it "should return all registers" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade
	 	end

	 	result = Shipment.all
	 	expect(result.length).to eq(2)
	 end


	 it "should return a ordered search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade
	 	end

	 	result = Shipment.all
	 	expect(result.length).to eq(2)
	 end


	 it "should return a ordered search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade
	 	end

	 	result = Shipment.order_by(:cd_lancamento).all
	 	expect(result.length).to eq(2)
	 end


	 it "should return a ordered desc search" do
	 	class Pinho::Shipment 
	 		extend Pinho::ODataFacade
	 	end

	 	result = Shipment.order_by_desc(:cd_lancamento).all
	 	expect(result.length).to eq(2)
	 end




  end
end
