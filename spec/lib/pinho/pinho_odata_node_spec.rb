require 'spec_helper'
require './lib/pinho/odata_facade'
require './lib/pinho/odata_node'

module Pinho
  describe ODataNode do

  	before :each do

 	 	stub_request(:get, "#{ENV['ODATA_API_URL']}/$metadata").
        	with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>"#{ENV['ODATA_API_HOST']}:80", 
        		'User-Agent'=>'Ruby'}).
         	to_return(:status => 200, :body => %Q{
         	<?xml version="1.0" encoding="utf-8"?>
				<edmx:Edmx Version="4.0" xmlns:edmx="http://docs.oasis-open.org/odata/ns/edmx">
				  <edmx:DataServices>
				    <Schema Namespace="ShipmentOData" xmlns="http://docs.oasis-open.org/odata/ns/edm">
				      <EntityType Name="Shipment">
				        <Key>
				          <PropertyRef Name="cd_lancamento" />
				        </Key>
				        <Property Name="cd_lancamento" Type="Edm.Int32" Nullable="false" />
				        <Property Name="cd_lancamento_two" Type="Edm.Int32" Nullable="false" />
				      </EntityType>
				    </Schema>
				    <Schema Namespace="Default" xmlns="http://docs.oasis-open.org/odata/ns/edm">
				      <EntityContainer Name="Container">
				        <EntitySet Name="Shipments" EntityType="ShipmentOData.Shipment" />
				      </EntityContainer>
				    </Schema>
				  </edmx:DataServices>
				</edmx:Edmx>
  		})

       stub_request(:get, "#{ENV['ODATA_API_URL']}/Shipments?$top=1").
         with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip,deflate', 'Host'=>"#{ENV['ODATA_API_HOST']}:80", 
         	'User-Agent'=>'Ruby'}).
         to_return(:status => 200, :body => %Q{
         	{
			  "@odata.context":"#{ENV['ODATA_API_URL']}/$metadata#Shipments","value":[
			    {
			      "cd_lancamento":21909,
			      "cd_lancamento_two":21909
			    }
			  ]
			}
         })

    end

    it "should return a valid top search" do
	 	class Shipment
	 		include Pinho::ODataNode
	 	end
	 	obj = Shipment.top(1).first
	 	expect(obj.cd_lancamento).to eq(21909)
	end


  end
end
