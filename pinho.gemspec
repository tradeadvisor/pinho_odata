$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "pinho/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "pinho_odata"
  s.version     = Pinho::VERSION
  s.authors     = ["Alvaro Cantador"]
  s.email       = ["alvaro.inckot@pinho.com.br"]
  s.homepage    = "http://pinho.com.br"
  s.summary     = "Pinho OData connection gem"
  s.description = "Connect to OData v4 services"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.6"

  s.add_dependency "rspec-rails", "~> 3.4.2"
  
end
