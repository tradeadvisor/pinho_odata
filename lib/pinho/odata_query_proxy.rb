module Pinho
	class ODataQueryProxy
		include Enumerable

		def initialize(klass)
			@connection = Pinho::ODataFacade.connection
			@klass = klass
			@query_builder = @connection.send(@klass.demodulize.pluralize)
		end

		def top(top)
			@query_builder.top(top)
			return self
		end

		def skip
			@query_builder.skip(top)
			return self
		end

		def filter(string_filter)
			@query_builder.filter(string_filter)
			return self
		end

		def order_by(field)
			@query_builder.order_by("#{field} asc")
			return self
		end

		def order_by_desc(field)
			@query_builder.order_by("#{field} desc")
			return self
		end

		def where(params)
			params.each do |key,value|
				if @connection.class_metadata[@klass.demodulize][key.to_s].type == "Edm.String"
					filter("#{key} eq '#{value}'")
				else
					filter("#{key} eq #{value}")
				end
			end
			return self
		end

		def all
			return bind
		end

		def each(&block)
			bind.each(&block)
		end

		private

			def bind
				@connection.execute
				Rails.logger.info(@connection.uri)
				Pinho::ODataFacade.bind(@klass, JSON.parse(@connection.response.body.to_s))
			end

	end
end