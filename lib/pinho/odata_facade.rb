require 'pinho/odata_query_proxy'

module Pinho
	module ODataFacade
		
		def self.connection(url = ENV['ODATA_API_URL'].dup)
			OData::Service.new(url)
		end

		def self.bind(klass, response)
			response["value"].map do |resp|
				obj = Object.const_get(klass).new
				obj.class.module_eval { attr_accessor :attributes }
				obj.attributes = resp
				next obj
			end
		end

		def where(params)
			query_proxy = Pinho::ODataQueryProxy.new(self.name)
			query_proxy.where(params)
			return query_proxy
		end

		def order_by(field)
			query_proxy = Pinho::ODataQueryProxy.new(self.name)
			query_proxy.order_by(field)
			return query_proxy
		end

		def order_by_desc(field)
			query_proxy = Pinho::ODataQueryProxy.new(self.name)
			query_proxy.order_by_desc(field)
			return query_proxy
		end

		def filter(filter_string)
			query_proxy = Pinho::ODataQueryProxy.new(self.name)
			query_proxy.filter(filter_string)
			return query_proxy
		end

		def all
			query_proxy = Pinho::ODataQueryProxy.new(self.name)
			return query_proxy.all
		end

		def top(top)
			query_proxy = Pinho::ODataQueryProxy.new(self.name)
			query_proxy.top(top)
			return query_proxy
		end

	end
end