require 'pinho/odata_facade'

module Pinho
	module ODataNode
	
		def self.included(base)
		   	base.extend(Pinho::ODataFacade)
		end

		def method_missing(name, *args, &blk)
		  	if args.empty? && blk.nil? && @attributes != nil && @attributes.has_key?(name.to_s)
		    	return @attributes[name.to_s]
		  	else
		    	super
		  	end	  
		end
		
	end
end